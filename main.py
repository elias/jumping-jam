import random
import time
import pygame


WIN_WIDTH = 800
WIN_HEIGHT = 600
FPS = 30
PIX = 10

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BROWN = (139, 69, 19)
BROWN_E = (100, 50, 50)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
DGREEN = (12, 128, 12)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
GREY = (128, 128, 128)
AQUA = (51, 255, 255)


class Human:
    def __init__(self, name, color_skin, color_shirt, color_shorts):
        self.name = name
        self.color_skin = color_skin
        self.color_shirt = color_shirt
        self.color_shorts = color_shorts
        self.x = PIX * 20
        self.y = WIN_HEIGHT / 3 * 2
        self.size = PIX * 5
        self.jump = False

    def jump_up(self):
        self.y -= PIX * 5

    def jump_down(self):
        self.y += PIX * 5

    def draw(self):
        body = pygame.Rect(self.x, self.y, self.size // 2, self.size)
        pygame.draw.rect(screen, self.color_shirt, body)

        head = pygame.Rect(self.x, self.y - PIX * 2, self.size // 2, PIX * 2)
        pygame.draw.rect(screen, self.color_skin, head)

        arm1 = pygame.Rect(self.x - PIX, self.y, PIX, PIX * 2)
        pygame.draw.rect(screen, self.color_skin, arm1)

        arm2 = pygame.Rect(self.x + self.size // 2, self.y, PIX, PIX * 2)
        pygame.draw.rect(screen, self.color_skin, arm2)

        short = pygame.Rect(self.x, self.y + PIX * 2, self.size // 2, PIX * 2)
        pygame.draw.rect(screen, self.color_shorts, short)

        legs = pygame.Rect(self.x, self.y + PIX * 4, self.size // 2, PIX)
        pygame.draw.rect(screen, self.color_skin, legs)


class Attacker:
    def __init__(self):
        self.x = PIX * 50
        self.y = WIN_HEIGHT / 3 * 2 + PIX * 3
        self.size = PIX * 2
        self.speed = 1
        self.color = RED
        self.crash = False
        self.falling = False

    def draw(self):
        picture = pygame.Rect(self.x, self.y, self.size, self.size)
        pygame.draw.rect(screen, self.color, picture)

    def move(self):
        self.x -= self.speed

    def fall(self):
        if self.x <= PIX * 8:
            self.x = PIX * 8
            self.falling = True

    def check_crash(self):
        if not human.jump:
            if human.x + human.size // 2 >= self.x >= human.x:
                attacker.crash = True
                return attacker.crash

            elif human.x + human.size // 2 >= self.x + self.size >= human.x:
                attacker.crash = True
                return attacker.crash


class Canon:
    def __init__(self):
        self.x = attacker.x
        self.y = attacker.y
        self.size = PIX * 3
        self.color = BLACK

    def draw(self):
        border = pygame.Rect(self.x, self.y - PIX / 2, self.size + PIX, PIX * 2)
        pygame.draw.rect(screen, self.color, border)

        inside = pygame.Rect(self.x + PIX / 2, self.y, self.size + PIX / 2, PIX + PIX / 2)
        pygame.draw.rect(screen, GREY, inside)

        legs = pygame.Rect(self.x + self.size - PIX, self.y + PIX, PIX, PIX)
        pygame.draw.rect(screen, BROWN, legs)


class Clouds:
    def __init__(self, cloud_x, cloud_y):
        self.x = cloud_x
        self.y = cloud_y
        self.sizeX = random.randint(PIX * 3, PIX * 6)
        self.sizeY = random.randint(PIX * 2, PIX * 4)
        self.speed = attacker.speed
        self.color = WHITE

    def draw(self):
        mass = pygame.Rect(self.x, self.y, self.sizeX, self.sizeY)
        pygame.draw.rect(screen, self.color, mass)

    def move(self):
        self.x -= self.speed


def speed(timer):
    if 0 <= timer <= 2:
        return 1
    elif 3 <= timer <= 6:
        return 2
    elif 7 <= timer <= 9:
        return 3
    elif 10 <= timer <= 14:
        return 4
    elif timer >= 15:
        return 5


def background():
    ground = pygame.Rect(0, WIN_HEIGHT / 2, WIN_WIDTH, WIN_HEIGHT / 2)
    pygame.draw.rect(screen, GREEN, ground)

    sky = pygame.Rect(0, 0, WIN_WIDTH, WIN_HEIGHT / 2 + PIX * 2)
    pygame.draw.rect(screen, AQUA, sky)


def platform():
    rect = pygame.Rect(PIX * 10, WIN_HEIGHT / 3 * 2 + human.size, WIN_WIDTH - PIX * 20, WIN_HEIGHT - PIX * 50)
    rect1 = pygame.Rect(WIN_WIDTH / 2 - PIX * 10, WIN_HEIGHT / 3 * 2 + human.size + WIN_HEIGHT - PIX * 50, PIX * 20, PIX * 5)
    pygame.draw.rect(screen, BLACK, rect)
    pygame.draw.rect(screen, BLACK, rect1)


def draw_move_clouds():
    cloud.draw()
    cloud.move()
    cloud1.draw()
    cloud1.move()
    cloud2.draw()
    cloud2.move()
    cloud3.draw()
    cloud3.move()
    cloud4.draw()
    cloud4.move()
    cloud5.draw()
    cloud5.move()
    cloud6.draw()
    cloud6.move()


def get_color(input_color):
    if input_color == '0':
        return WHITE
    elif input_color == '1':
        return BLACK
    elif input_color == '2':
        return RED
    elif input_color == '3':
        return DGREEN
    elif input_color == '4':
        return BLUE
    elif input_color == '5':
        return YELLOW


if input('Do you want to use the standard character? (Y/N)') == 'N':
    print('WHITE = 0\n BLACK = 1\n RED = 2\n GREEN = 3\n BLUE = 4\n YELLOW = 5\n')

    human = Human('Alex', get_color(input('Skin color: ')), get_color(input('Shirt color: ')), get_color(input('Shorts color: ')))
else:
    human = Human('Alex', WHITE, RED, BLUE)
attacker = Attacker()
canon = Canon()

cloud = Clouds(PIX * 10, PIX * 7)
cloud1 = Clouds(PIX * 21, PIX * 8)
cloud2 = Clouds(PIX * 34, PIX * 3)
cloud3 = Clouds(0, PIX * 11)
cloud4 = Clouds(PIX * 45, PIX * 9)
cloud5 = Clouds(PIX * 78, PIX * 12)
cloud6 = Clouds(PIX * 23, PIX * 15)

pygame.init()
screen = pygame.display.set_mode((WIN_WIDTH, WIN_HEIGHT))
pygame.display.set_caption('Jumping Jam')
clock = pygame.time.Clock()
music = pygame.mixer.Sound('music.wav')
ending = pygame.mixer.Sound('ending.wav')
music.set_volume(0.06)
ending.set_volume(0.1)
music.play()

counter = 0
fallCount = 0
fallCount1 = 0
speedlimit = 0
attackCount = 0
play_sound = True
create = False

while True:
    for i in pygame.event.get():
        if i.type == pygame.QUIT:
            pygame.quit()
            break
        if i.type == pygame.KEYDOWN:
            if i.key == pygame.K_SPACE:
                if not human.jump:
                    human.jump_up()
                    human.jump = True

    screen.fill(AQUA)
    attacker.check_crash()
    if create:
        attacker1.check_crash()

    if not attacker.crash:
        if human.jump:
            counter += 1
            if counter == 60 / speed(speedlimit):
                human.jump_down()
                counter = 0
                human.jump = False

        if speedlimit >= 3:
            attackCount += 1

        if attackCount == 90:
            attacker1 = Attacker()
            attacker1.speed = attacker.speed
            create = True

        attacker.fall()
        if attacker.falling:
            fallCount += 1
            if fallCount <= 40:
                attacker.y += PIX
                if attacker.y > WIN_HEIGHT and attacker.x == 80:
                    attacker.falling = False
                    attacker.y = WIN_HEIGHT / 3 * 2 + PIX * 3
                    attacker.x = PIX * 50
                    fallCount = 0
                    speedlimit += 1

        if create:
            attacker1.fall()
            if attacker1.falling:
                fallCount1 += 1
                if fallCount1 <= 40:
                    attacker1.y += PIX
                    if attacker1.y > WIN_HEIGHT and attacker1.x == 80:
                        attacker1.falling = False
                        attacker1.y = WIN_HEIGHT / 3 * 2 + PIX * 3
                        attacker1.x = PIX * 50
                        fallCount1 = 0
                        speedlimit += 1

        if cloud.x <= -cloud.sizeX:
            cloud.x = WIN_WIDTH
        elif cloud1.x <= -cloud1.sizeX:
            cloud1.x = WIN_WIDTH
        elif cloud2.x <= -cloud2.sizeX:
            cloud2.x = WIN_WIDTH
        elif cloud3.x <= -cloud3.sizeX:
            cloud3.x = WIN_WIDTH
        elif cloud4.x <= -cloud4.sizeX:
            cloud4.x = WIN_WIDTH
        elif cloud5.x <= -cloud5.sizeX:
            cloud5.x = WIN_WIDTH
        elif cloud6.x <= -cloud6.sizeX:
            cloud6.x = WIN_WIDTH

        attacker.speed = speed(speedlimit)
        if create:
            attacker1.speed = speed(speedlimit)

        background()
        platform()

        draw_move_clouds()
        human.draw()
        attacker.draw()
        canon.draw()
        if create:
            attacker1.draw()
            attacker1.move()
        attacker.move()

        pygame.display.update()
        clock.tick(FPS)

    else:
        music.stop()
        if play_sound:
            ending.play()
        screen.fill(WHITE)
        font = pygame.font.SysFont(None, 50)
        img = font.render('GAME OVER !', True, RED)
        games = font.render('You passed ' + str(speedlimit) + ' levels', True, BLUE)
        screen.blit(img, (WIN_WIDTH / 2 - PIX * 12, WIN_HEIGHT / 2 - PIX * 6))
        screen.blit(games, (WIN_WIDTH / 2 - PIX * 16 + PIX / 2, WIN_HEIGHT / 2 - PIX * 2))
        pygame.display.update()
        clock.tick(FPS)
        if not play_sound:
            pygame.quit()

        if play_sound:
            time.sleep(4)
            ending.stop()
            play_sound = False
